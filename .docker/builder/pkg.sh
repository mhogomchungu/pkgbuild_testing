#!/bin/bash

if [ ! -f PKGBUILD ]; then
  echo "No PKGBUILD found. Exiting."
  exit 1
fi

echo ">> Updating base"
pacman -Sy --noprogressbar --noconfirm
pacman -S --noprogressbar --noconfirm archlinux-keyring
pacman -Su --noprogressbar --noconfirm

echo ">> Cleaning up the package folder"
rm -rf src pkg *.pkg.tar.xz

echo ">> Installing dependencies and building package"
deps=$(awk '/depends/{a=1} a; /)/{a=0}' PKGBUILD | sed -n "/optdepends/q;p" | sed "s/^[^=]*=(//;s/)//g;s/'//g" | tr '\n' ' ')
expect_install.exp sudo -u maker yay -S --needed $deps
sudo -u maker makepkg --cleanbuild --clean --force --skippgpcheck

echo ">> Checking package with namcap"
result=$(find *.pkg.tar.zst)
namcap $result

echo ">> Testing if package can be installed"
yes | pacman -U $result
