#!/usr/bin/env xonsh

import os
import re
from ruamel.yaml import YAML
yaml = YAML()

print('Pushing commit', $CI_COMMIT_TITLE)
folder = [i for i in os.listdir() if not re.search(r'^\.', i) and re.search(re.compile(i + "[\s_]"), $CI_COMMIT_TITLE)][0]
print('Entering folder', folder)

os.chdir(folder)

with open(".info.yaml", 'r') as file:
    text = file.readlines()

text = [ln for ln in text if not re.search("{", ln)]
details = yaml.load("\n".join(text))

url = details['repos']['aur']['url']
git clone @(url) aur
os.chdir("aur")

for file in details['files']:
    print("Collecting", file)
    os.rename(os.path.realpath("..") + "/" +  file,
              os.path.realpath(".") + "/" + file)
    git add @(file)

git config user.name "Conor Anderson (bot)"
git config user.email "bot@conr.ca"
git commit -m "$CI_COMMIT_TITLE 🤖"
git push --set-upstream origin master
