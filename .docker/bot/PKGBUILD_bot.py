import os
from jinja2 import Template
from ruamel.yaml import YAML
import subprocess
import re
import requests
import gitlab
import warnings
yaml = YAML()


def gitactions(details, newver, project, b_name):
    branch = project.branches.create({'branch': b_name,
                                      'ref': 'master'})
    data = {
        'branch': b_name,
        'commit_message': details['name'] + ' version -> ' + newver,
        'actions': []
    }
    
    for file in details['files']:
        filename = details['name'] + "/" + file
        dict = {'action': 'update', 'file_path': filename, 'content': open(filename).read()}
        data['actions'].append(dict)
    
    data['actions'].append({'action': 'update', 'file_path': yf, 'content': open(yf).read()})
    commit = project.commits.create(data)
    mr = project.mergerequests.create({'source_branch': b_name,
                                       'target_branch': 'master',
                                       'title': "Update {} to version {}.".format(details['name'], newver),
                                       'labels': ['bot', 'bump']})


def update_files(details, newver):
    with open(details['name'] + "/PKGBUILD", 'r') as file:
        lines = file.readlines()
        
    pkgver = details['pkgver']['override'] if "override" in details['pkgver'] else "pkgver"
    
    for ln in range(0, len(lines)):
        if re.match(pkgver, lines[ln]): 
            lines[ln] = pkgver + "=" + str(newver) + "\n"
        if re.match("pkgrel", lines[ln]): 
            lines[ln] = "pkgrel=1\n"
    
    with open(details['name'] + "/PKGBUILD", 'w') as file:
        file.writelines(lines)
    
    subprocess.call(
        ("cd " + details['name'] + " && sudo -u maker updpkgsums && exit 0"),
        stderr=subprocess.STDOUT,
        shell=True)
    
    srcinfo = subprocess.check_output(
        ("cd " + details['name'] + " && sudo -u maker makepkg --printsrcinfo && exit 0"),
        stderr=subprocess.STDOUT,
        shell=True)
    
    with open(details['name'] + "/.SRCINFO", 'w') as file:
        file.write(srcinfo.decode())


gl = gitlab.Gitlab("https://gitlab.com", private_token=os.environ["GLPAT"])
project = gl.projects.get('ConorIA/pkgbuild_testing')

dirs = [i for i in os.listdir() if not re.search(r'\.|-git$', i)]


for d in dirs:
    print("Checking updates for", d)
    yf = d + "/.info.yaml"
    
    with open(yf, 'r') as file:
        text = file.read()

    t = Template(text)
    t_rendered = t.render()
    details = yaml.load(t_rendered)
    
    if details['repos']['upstream']['type'] == 'gh':
        query = details['repos']['upstream']['rels'] if 'rels' in details['repos']['upstream'] else "releases/latest"
        r = requests.get('https://api.github.com/repos/' + details['repos']['upstream']['repo'] + '/' + query)
    else:
        # Other upstreams need to be added here
        warnings.warn("We don't have a handler for this upstream type yet. Skipping.")
        continue

    newver = r.json()['tag_name'] if 'tag_name' in r.json() else str(r.json())

    for rg in details['regex']:
        newver = re.findall(rg, newver)
        if newver == []:
            warnings.warn("We couldn't find a version number. Check your regex. Skipping.")
            continue
        else:
            newver = newver[0]

    if newver == details['pkgver']['ver']:
        print("Version number hasn't changed.")
        continue

    ## Update the version number in the .info.yaml
    lines = text.splitlines()
    lines[0] = "{% set version = '" + newver + "' %}"
    with open(yf, 'w') as file:
        file.write("\n".join(lines))

    ## Check if we already have a branch for this version
    b_name = details['name'] + "_" + newver
    try:
        branch = project.branches.get(b_name)
    except Exception as e:
        if not re.search(r'404 Branch Not Found', str(e)):
            sys.exit("Got an unexpected error")
        else:
            branch = "new"

    if branch == "new":
        update_files(details, newver)
        gitactions(details, newver, project, b_name)


print("Finished checking for updates!")
