# PKGBUILD Testing

This repo was created as a learning experience. 

It consists of three Docker images and my PKGBUILD files. 

1. bot checks if upstream has released a new version of the software in a PKGBUILD.
2. builder checks to see if a build succeeds for the new `pkver`.
3. pusher clones the AUR repo for the PKGBUILD, and pushes the changes.
